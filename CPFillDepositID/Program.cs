﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using Utilities;

namespace CPFillDepositID
{
    class Program
    {
        static int Main(string[] args)
        {
            DateTime pgmStart = DateTime.Now;
            PageInfo pi = new PageInfo();
            DateTime dt = DateTime.Now;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            string sqlForDetail = string.Empty;
            string sqlForUpdate = string.Empty;

            decimal cpTotal = 0;
            decimal achTot = 0;
            decimal ccTot = 0;
            decimal lb2tot = 0;
            decimal lb2Ch = 0;
            int cpBatchId = 0;
            int cpTotalCount = 0;
            decimal mriTotal = 0;

            string rowId = string.Empty;
            string CurrentUserEmail = string.Empty;

            pi.LogFileName = string.Format(@"C:\HillMgt Automation\HillEnterpriseReporting\CPFillDepositID\{0}{1}{2}.log",
                dt.Year.ToString(), dt.Month.ToString("D2"), dt.Day.ToString("D2"));

            pi.EchoToConsole = true;
            Logger logger = new Logger(pi);
            SqlAgent sa = new SqlAgent(logger);
            ReportDBAgent ra = new ReportDBAgent(logger);

            // Get the period from args[0]
            if (args.Count() < 1)
            {
                logger.LogMessage("Invalid Arguments");
                return (-1);
            }

            try
            {
                // Read up a record from the reporting database HS_PROCESSIBNG
                rowId = args[0];

                DataSet ds = ra.GetSpecificRow(rowId);

                // Set properties from HS_PROCESSING record
                string parms = ds.Tables[0].Rows[0]["Parms"].ToString().Trim();
                string[] ps = parms.Split('|');
                pi.Period = ps[0];
                string modeOfOperation = ps[1]; //D for detail, S for Summary
                string userId = ds.Tables[0].Rows[0]["UserId"].ToString();
                CurrentUserEmail = ds.Tables[0].Rows[0]["Email"].ToString();
                logger.LogMessage(string.Format("Current User's Email: {0}", CurrentUserEmail));

                // This program takes longer than a minute to run.  We need to set this status column
                // to something other than Ready so the next time the listener starts up, this program
                // is not executed again.  Would lead to the server crashing.
                ra.SetInProgress(rowId);
                ds.Dispose();
            }
            catch(Exception ex)
            {
                logger.LogMessage(string.Format("An error occurred while reading the reporting database:  {0}", ex.Message));
            }

            //pi.Period = args[0];
            FixupPeriods(pi);
            startDate = new DateTime(int.Parse(pi.Period.Substring(0, 4)), int.Parse(pi.Period.Substring(4,2)), 1);
            endDate = startDate.AddMonths(3);
            startDate = startDate.AddMonths(-2);
            logger.LogMessage("Process CPFillDepositID has started.");
            logger.LogMessage(string.Format("Running for date: {0}", startDate.ToLongDateString()));

            pi.ExcelFileName = string.Format(@"C:\HillMgt Automation\HillEnterpriseReporting\CPFillDepositID\CPFillDepositID_{0}.xlsx", pi.Period);

            logger.LogMessage("Running PreProcessor");
            PreProcessRMBTCH(logger);

            // This loop runs a specific database process described below for each day in the 
            // specified period.  (startDate and endDate)
            DateTime loopDate = startDate;

            try
            {
                ExcelAgent ea = new ExcelAgent(logger, pi);
                ea.ApplyInitialFormat();
                while (true)
                {
                    logger.LogMessage(string.Format("Processing date: {0}", loopDate.ToShortDateString()));

                    // The process is, we execute a SQL that will return two tables.
                    // One is from  ClickPay that has the total amount of a batch being processed.
                    // The other is a query from RMBTCH that sums the amounts of batch rows.
                    string sql =
                        "select BatchID, Merchant, TotalAmount, AchAmount, CreditCardAmount, LBXACHAmount, LBXCHK21Amount " +
                        "from HS_ClickPay_Deposit_RM " +
                        "where date = '{0}' ";
                    sql = string.Format(sql, loopDate);

                    DataSet dsCP = sa.ExecuteQuery(sql);

                    foreach (DataRow dr in dsCP.Tables[0].Rows)
                    {
                        cpBatchId = int.Parse(dr["BatchID"].ToString());
                        cpTotal = decimal.Parse(dr["TotalAmount"].ToString());
                        achTot = decimal.Parse(dr["ACHAmount"].ToString());
                        ccTot = decimal.Parse(dr["CreditCardAmount"].ToString());
                        lb2tot = decimal.Parse(dr["LBXACHAmount"].ToString());
                        lb2Ch = decimal.Parse(dr["LBXCHK21Amount"].ToString());
                        string desc = dr["Merchant"].ToString();

                        // Write a line  to the Excel  File
                        ea.WriteClickPayTotal(cpBatchId.ToString(), loopDate, cpTotalCount, cpTotal, achTot, ccTot, lb2tot, lb2Ch);

                        if (desc.IndexOf("appfee".ToLower()) >= 0)
                        {
                            sql =
                                "select sum(coalesce(crctrl, 0) - coalesce(nsfctrl, 0)) as TotalAmount  " +
                                "from RMBTCH " +
                                "where BATCHDATE = '{0}' " +
                                "and descrptn = 'PC CREDITCARD' ";
                            sqlForDetail =
                                "select b.rmbatchid, b.batchdate, b.descrptn, " +
                                "   coalesce(B.CRCTRL, 0) - coalesce(NSFCTRL, 0) as LineAmount, " +
                                "   l.PERIOD, bankrecid " +
                                "from RMBTCH as b " +
                                "   join rmledg as l on l.rmbatchid = b.rmbatchid " +
                                "   right outer join RMDEPOSIT as T on t.rmbatchid = b.rmbatchid " +
                                "where HS_CP_DepositID = {0} and t.status not in ('O') " +
                                "group by " +
                                "   b.RMBATCHID, b.batchdate, b.descrptn, bankrecid, coalesce(B.CRCTRL, 0) - coalesce(NSFCTRL, 0), " +
                                "   l.PERIOD ";
                            sqlForDetail = string.Format(sqlForDetail, cpBatchId);
                            sqlForUpdate =
                                "update rmbtch " +
                                "set hs_cp_depositid = {0} " +
                                "where CAST(batchdate AS DATE) = '{1}' " +
                                "and descrptn = 'PC CREDITCARD' ";
                            sqlForUpdate = string.Format(sqlForUpdate, cpBatchId, loopDate.ToShortDateString());
                        }
                        else
                        {
                            sql =
                                "select t.bankrecid, r.period, " +
                                "   sum(coalesce(B.CRCTRL, 0) - coalesce(NSFCTRL, 0)) as TotalAmount " +
                                "from RMBTCH b " +
                                "        inner join RMDEPOSIT t on b.rmbatchid = t.rmbatchid " +
                                "        inner join AP_BANKREC r on r.bankrecid = t.bankrecid " +
                                "where HS_DepositDate = '{0}' " +
                                "and partnername<> 'Conservice' " +
                                "group by t.bankrecid, r.period ";
                            sqlForDetail =
                                "select b.rmbatchid, b.batchdate, b.descrptn,  " +
                                "   coalesce(B.CRCTRL, 0) - coalesce(NSFCTRL, 0) as LineAmount, " +
                                "	l.PERIOD, bankrecid " +
                                "from RMBTCH as b " +
                                "join rmledg as l on l.rmbatchid = b.rmbatchid " +
                                "right outer join RMDEPOSIT as T on t.rmbatchid = b.rmbatchid " +
                                "where HS_CP_DepositID = {0} and t.status not in ('O') " +
                                "group by " +
                                "    b.RMBATCHID, b.batchdate, b.descrptn, bankrecid, " +
                                "	coalesce(B.CRCTRL, 0) - coalesce(NSFCTRL, 0),  " +
                                "	l.PERIOD ";
                            sqlForDetail = string.Format(sqlForDetail, cpBatchId);
                            sqlForUpdate =
                                "update rmbtch " +
                                "set hs_cp_depositid = {0} " +
                                "where CAST(HS_DepositDate AS DATE) = '{1}' " +
                                "and partnername <> 'Conservice' ";
                            sqlForUpdate = string.Format(sqlForUpdate, cpBatchId, loopDate.ToShortDateString());
                        }

                        sql = string.Format(sql, loopDate);
                        DataSet dsMRI = sa.ExecuteQuery(sql);

                        if (dsMRI.Tables[0].Rows.Count != 0)
                        {
                            if (dsMRI.Tables[0].Rows[0]["TotalAmount"] != DBNull.Value)
                                mriTotal = decimal.Parse(dsMRI.Tables[0].Rows[0]["TotalAmount"].ToString());
                            else
                                mriTotal = 0;
                        }

                        // Run the update
                        sa.ExecuteNonQuery(sqlForUpdate);

                        // Now get the un-summarized detail information from RMBTCH
                        DataSet dsDetail = sa.ExecuteQuery(sqlForDetail);
                        ea.BeginMRIBatch();
                        foreach (DataRow drDetail in dsDetail.Tables[0].Rows)
                        {
                            decimal tot = 0;
                            if (drDetail["LineAmount"] != DBNull.Value)
                                tot = decimal.Parse(drDetail["LineAmount"].ToString());

                            ea.WriteMRIDetail(drDetail["rmbatchid"].ToString(),
                                DateTime.Parse(drDetail["batchdate"].ToString()), tot, drDetail["Descrptn"].ToString(),
                                drDetail["bankrecid"].ToString());

                        }
                        ea.CreateMRITotal();
                    }

                    // Bump the loop date
                    loopDate = loopDate.AddDays(1);

                    // If the loop date has a day = 1, we're in the next month.  Bail.
                    if (loopDate == endDate)
                        break;
                }

                ea.WriteTransTotals();
                ea.SaveAndClose(pi);
            }
            catch (Exception ex)
            {
                logger.LogMessage(string.Format("An error occurred: {0}", ex.Message));
                ra.SetFailed(ex.Message, rowId);
                return (-1);
            }


            logger.LogMessage("Process CPFillDepositID has ended.");
            logger.LogMessage(string.Format("Total Execution Time: {0}", (DateTime.Now - pgmStart).TotalMinutes));

            // Send Email with this new file attached.
            EmailAgent em = new EmailAgent(logger);

            // Only send the email if an attachment has been created.
            if (File.Exists(pi.ExcelFileName))
                em.SendEmail(pi.ExcelFileName, CurrentUserEmail);

            ra.SetComplete(rowId);
            return (0);
        }

        public static void FixupPeriods(PageInfo pi)
        {
            DateTime dtCurrent = new DateTime(int.Parse(pi.Period.Substring(0, 4)), int.Parse(pi.Period.Substring(4, 2)), 1);
            DateTime dtPast = dtCurrent.AddMonths(-1);
            DateTime dtWayBack = dtCurrent.AddMonths(-2);
            DateTime dtFuture = dtCurrent.AddMonths(1);
            DateTime dtWayFore = dtCurrent.AddMonths(2);

            // 01/01/2020
            // 0--3--6---
            pi.PeriodWayBack = string.Format("{0}{1}", dtWayBack.Year.ToString(), dtWayBack.Month.ToString("D2"));
            pi.PeriodPast = string.Format("{0}{1}", dtPast.Year.ToString(), dtPast.Month.ToString("D2"));
            pi.PeriodFuture = string.Format("{0}{1}", dtFuture.Year.ToString(), dtFuture.Month.ToString("D2"));
            pi.PeriodWayFore = string.Format("{0}{1}", dtWayFore.Year.ToString(), dtWayFore.Month.ToString("D2"));
        }

        private static void PreProcessRMBTCH(Logger logger)
        {
            try
            {
                SqlAgent sa = new SqlAgent(logger);
                
                string bulkUpdateCommandText = string.Empty;
                int counter = 1;



                //---------------------
                // John Hall's Fixes
                //---------------------
                sa.ExecuteNonQuery("update rmbtch set hs_depositDate = Null");
                string updateSQL = "" +
                    "update rmbtch set hs_depositDate = Settlement_date " +
                    "from RMBTCH as b " +
                        "join hs_clickpay_detailed_rm as d on b.RMBATCHID = d.Software_BatchID " +
                    "where HS_DepositDate <> Settlement_Date  ";                   // These are the various values that could be in the description field of RMBATCH.
                sa.ExecuteNonQuery(updateSQL);
                //---------------------




                DataSet ds = sa.ExecuteQuery("Select rmbatchid, descrptn from rmbtch where hs_depositdate is null ");

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    DateTime dt = DateTime.MinValue;
                    string updateString = string.Empty;

                    // These are the various values that could be in the description field of RMBATCH.
                    // It's intentionally verbose for readability.
                    try
                    {
                        string sDt = string.Empty;

                        //if (dr[1].ToString().IndexOf('/') >= 0)
                        //{
                        //    sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('/') - 2, 10).Trim();
                        //    updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                        //}


                        if (dr[1].ToString().IndexOf('/') >= 0 || dr[1].ToString().IndexOf('-') >= 0)
                        {
                            if (dr[1].ToString().Substring(0, 2) == "CP")
                            {
                                sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('/') - 2, 10).Trim();
                                updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                            }
                            if (dr[1].ToString().Substring(0, 10) == "Conservice")
                            {
                                sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('-') - 4, 10).Trim();
                                string sDt2 = string.Format("{0}/{1}/{2}",
                                    sDt.Substring(5, 2), sDt.Substring(8, 2), sDt.Substring(0, 4));
                                updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt2, dr[0].ToString());
                            }
                            if (dr[1].ToString().Substring(0, 3) == "RTN")
                            {
                                sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('/') - 2, 10).Trim();
                                updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                            }
                            if (dr[1].ToString().Substring(0, 3) == "RVS")
                            {
                                sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('/') - 2, 10).Trim();
                                updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                            }
                            if (dr[1].ToString().Substring(0, 8) == "ClickPay")
                            {
                                if (dr[1].ToString().IndexOf('/') >= 0)
                                {
                                    sDt = dr[1].ToString().Substring(dr[1].ToString().IndexOf('/') - 2, 10).Trim();
                                    updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                                }
                                else
                                {
                                    string tmp = dr[1].ToString().Substring(dr[1].ToString().IndexOf('-') - 4, 10).Trim();
                                    sDt = string.Format("{0}/{1}/{2}", tmp.Substring(5, 2), tmp.Substring(8, 2), tmp.Substring(0, 4));
                                    updateString = string.Format("update rmbtch set HS_DepositDate = '{0}' where RMBATCHID = '{1}' ", sDt, dr[0].ToString());
                                }
                            }
                        }
                    }
                    catch
                    {
                        continue;
                    }

                    // Each iteration of the loop in which a date was found, concatinate an
                    // UPDATE statement to the end of bldUpdateCommandText
                    if (updateString != string.Empty)
                    {
                        bulkUpdateCommandText += updateString;

                        // Every 100 rows, perform a queryy and reset bulkUpdateCommandText
                        if (counter++ % 100 == 0)
                        {
                            logger.LogMessage(counter.ToString());
                            sa.ExecuteNonQuery(bulkUpdateCommandText);
                            bulkUpdateCommandText = string.Empty;
                        }
                    }
                }

                // Now that the entire loop is complete, execute bulk UPDATE statement we've constructed.
                if (bulkUpdateCommandText != string.Empty)
                    sa.ExecuteNonQuery(bulkUpdateCommandText);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
