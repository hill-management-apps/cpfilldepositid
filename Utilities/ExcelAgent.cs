﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;

namespace Utilities
{

    public class ExcelAgent
    {
        private const int CP_TOTAL_COL = 15;
        private const int MRI_TOTAL_COL = 15;
        private const int BANKRECID_COL = 17;

        private int currentRow = 1;
        private int beginBatchRow = 0;
        private int numberOfRowsInBatch = 0;
        private string beginBatchRowLetter = "O";
        private string beginPeriodLetter = "Q";
        private decimal lastCPTotal = 0;

        private Application app;
        private Workbook wb;
        private Worksheet ws;

        private Logger logger = null;
        private PageInfo pi = null;
        private List<float> cw = null;
        private List<string> periods = null;
        private DateTime periodHigh = DateTime.MinValue;
        private DateTime periodLow = DateTime.MinValue;
        private string[] transPeriods = new string[5];

        // These variables are for tracking and displaying
        // the Bank Deposit IDs at the bottom of the report
        private Dictionary<string, decimal[]> grandTotalsByBankRecID = new Dictionary<string, decimal[]>();
        private const int BANKRECID_SUMMARY_COLUMN = 3;
        private const int BANKREC_DEPOSIT_CENTER_COLUMN = 7;

        public ExcelAgent(Logger sfLogger, PageInfo _pi)
        {
            logger = sfLogger;
            pi = _pi;

            transPeriods[0] = pi.PeriodWayBack;
            transPeriods[1] = pi.PeriodPast;
            transPeriods[2] = pi.Period;
            transPeriods[3] = pi.PeriodFuture;
            transPeriods[4] = pi.PeriodWayFore;
        }

        #region Create App, Workbook and Worksheet Objects
        public void ApplyInitialFormat()
        {
            Range r = null;

            try
            {
                logger.LogMessage("ExcelAgent.ApplyInitialFormat");

                // Previous sheet name being empty indicates the 
                // first time thru
                // Create the  necessary junk for Excel to run
                app = new ApplicationClass();       // App
                                                    //app.DisplayAlerts = false;
                                                    //app.ScreenUpdating = false;

                //===================
                // Visible
                //===================
                //app.Visible = true;
                //app.UserControl = true;
                //app.Interactive = true;

                //===================
                // Invisible
                //===================
                app.DisplayAlerts = false;
                app.ScreenUpdating = false;
                app.Visible = false;
                app.UserControl = false;
                app.Interactive = false;


                wb = app.Workbooks.Add();           // Workbook
                ws = (Worksheet)wb.Sheets["Sheet1"];

                // Write the header
                ws.Cells[1, 2] = "ClickPay vs. MRI";
                r = ((Range)ws.Cells[1, 2]);
                r.Font.Bold = true;
                r.Font.Size = 14;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[2, 1] = "Batch Reconciliation Report for Period:";
                r = ((Range)ws.Cells[2, 1]);
                r.Font.Bold = false;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[2, 5] = pi.Period;
                r = ((Range)ws.Cells[2, 5]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[3, BANKRECID_COL] = "MRI BankRecId";
                r = ((Range)ws.Cells[3, BANKRECID_COL]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[3, BANKRECID_COL+1] = "       MRI Trans Period";
                r = ((Range)ws.Cells[3, BANKRECID_COL + 1]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[4, BANKRECID_COL + 1] = pi.FormattedPeriodWayBack;
                r = ((Range)ws.Cells[4, BANKRECID_COL + 1]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                ws.Cells[4, BANKRECID_COL + 2] = pi.FormattedPeriodPast;
                r = ((Range)ws.Cells[4, BANKRECID_COL + 2]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                ws.Cells[4, BANKRECID_COL + 3] = pi.FormattedPeriod;
                r = ((Range)ws.Cells[4, BANKRECID_COL + 3]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                ws.Cells[4, BANKRECID_COL + 4] = pi.FormattedPeriodFuture;
                r = ((Range)ws.Cells[4, BANKRECID_COL + 4]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                ws.Cells[4, BANKRECID_COL + 5] = pi.FormattedPeriodWayFore;
                r = ((Range)ws.Cells[4, BANKRECID_COL + 5]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                // Shortfall
                ws.Cells[3, BANKRECID_COL - 1] = "Shortfall";
                r = ((Range)ws.Cells[3, BANKRECID_COL - 1]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;


                // Column Widths
                r = ((Range)ws.Cells[currentRow, CP_TOTAL_COL]);
                r.ColumnWidth = 18;

                r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL]);
                r.ColumnWidth = 18;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL]);
                r.ColumnWidth = 15;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 1]);
                r.ColumnWidth = 14;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 2]);
                r.ColumnWidth = 14;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 3]);
                r.ColumnWidth = 14;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 4]);
                r.ColumnWidth = 14;

                r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 5]);
                r.ColumnWidth = 14;

                // Shortfall
                r = ((Range)ws.Cells[currentRow, BANKRECID_COL - 1]);
                r.ColumnWidth = 14;

                // Blank Columns in center.  Make 'em small.
                r = ((Range)ws.Cells[currentRow, 11]); ;
                r.ColumnWidth = 1;
                r = ((Range)ws.Cells[currentRow, 12]); ;
                r.ColumnWidth = 1;
                r = ((Range)ws.Cells[currentRow, 13]); ;
                r.ColumnWidth = 1;

                // Set the row for the first bit of data.
                currentRow = 5;
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.ApplyInitialFormat: {0}", _ex.Message));
                throw _ex;
            }
        }
        #endregion

        public void WriteClickPayTotal(string batchID, DateTime batchDate, int itemCount, decimal totalAmt, decimal achTot, decimal ccTot, decimal lb2Tot, decimal lbTotCh)
        {
            string outLine = string.Format("ClickPay Batch {0} occurred on {1} -    ACH: {2}       CC: {3}       LBCH: {4}       LBCHK2: {5}", 
                batchID, batchDate.ToShortDateString(), achTot.ToString("C"), ccTot.ToString("C"), lb2Tot.ToString("C"), lbTotCh.ToString("C"));

            ws.Cells[currentRow, 1] = outLine;
            Range r = ((Range)ws.Cells[currentRow, 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;

            ws.Cells[currentRow, CP_TOTAL_COL] = totalAmt.ToString("C");
            r = ((Range)ws.Cells[currentRow, CP_TOTAL_COL]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.ColumnWidth = 18;

            lastCPTotal = totalAmt;
            currentRow++;
        }

        public void WriteMRIDetail(string batchID, DateTime batchDate, decimal totalAmt, string desc, string bankRecID)
        {
            SqlAgent sa = null;
            DataSet ds = null;

            string outLine = string.Format(" was posted on {0} - {1}", batchDate.ToShortDateString(), desc);

            ws.Cells[currentRow, 2] = "Batch: ";
            Range r = ((Range)ws.Cells[currentRow, 2]);
            r.Font.Bold = false;
            r.Font.Size = 10;

            ws.Cells[currentRow, 3] = batchID;
            r = ((Range)ws.Cells[currentRow, 3]);
            r.Font.Bold = false;
            r.Font.Size = 10;

            ws.Cells[currentRow, 4] = outLine;
            r = ((Range)ws.Cells[currentRow, 4]);
            r.Font.Bold = false;
            r.Font.Size = 10;


            ws.Cells[currentRow, MRI_TOTAL_COL] = totalAmt.ToString("C");
            r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL]);
            r.Font.Bold = false;
            r.Font.Size = 10;

            try
            {
                string sql =
                    "select l.rmbatchid, l.period, -sum(tranamt) as TotAmt " +
                    "from rmledg l " +
                    "    right outer join RMDEPOSIT as T on t.rmbatchid = l.rmbatchid " +
                    "where l.rmbatchid = '{0}' " +
                    "and srccode in ('cr', 'ns', 'pr') " +
                    "and t.status not in ('O') " +
                    "group by l.rmbatchid, l.period " +
                    "order by l.rmbatchid desc, l.period desc ";
                sql = string.Format(sql, batchID);

                sa = new SqlAgent(logger);
                ds = sa.ExecuteQuery(sql);

                // do maintenance on grand totals 
                // if the dictionary DOES NOT contain the bankrecid
                // add an item to the dictionary and initialize the array
                // to have 4 elements all zero.
                if (!grandTotalsByBankRecID.ContainsKey(bankRecID))
                {
                    grandTotalsByBankRecID.Add(bankRecID, new decimal[] { 0, 0, 0, 0, 0, 0 });
                }


                ws.Cells[currentRow, BANKRECID_COL] = bankRecID;
                r = ((Range)ws.Cells[currentRow, BANKRECID_COL]);
                r.Font.Bold = false;
                r.Font.Size = 10;

                decimal runningTot = 0;

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    decimal tot = decimal.Parse(dr["TotAmt"].ToString());
                    runningTot += tot;
                    int columnOffset = DeterminePeriodColumn(dr["period"].ToString());

                    if (columnOffset <= transPeriods.Length)
                    {
                        grandTotalsByBankRecID[bankRecID][columnOffset] += tot;
                        ws.Cells[currentRow, BANKRECID_COL + columnOffset] = tot.ToString("C");
                        r = ((Range)ws.Cells[currentRow, BANKRECID_COL]);
                        r.Font.Bold = false;
                        r.Font.Size = 10;
                    }
                }

                if (runningTot != totalAmt)
                {
                    ws.Cells[currentRow, BANKRECID_COL + 4] = (totalAmt - runningTot).ToString("C");
                    r = ((Range)ws.Cells[currentRow, BANKRECID_COL + 4]);
                    r.ColumnWidth = 14;
                    r.Font.Color = Color.Red;
                    r.Font.Bold = true;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if(ds != null)
                    ds.Dispose();

                sa = null;
            }

            numberOfRowsInBatch++;
            currentRow++;
        }

        private int DeterminePeriodColumn(string period)
        {
            for (int i = 0; i < 5; i++)
            {
                if (transPeriods[i] == period)
                    return i + 1;
            }
            return 6;
        }

        public void BeginMRIBatch()
        {
            beginBatchRow = currentRow;
            numberOfRowsInBatch = 0;
        }

        public void CreateMRITotal()
        {
            Range r;

            if (numberOfRowsInBatch != 0)
            {
                // Create a formula
                string beg = string.Format("{0}{1}", beginBatchRowLetter, beginBatchRow.ToString());
                string end = string.Format("{0}{1}", beginBatchRowLetter, (currentRow - 1).ToString());
                string formula = string.Format("=SUM({0}:{1})", beg, end);

                // And set the cell value
                ws.Cells[currentRow, MRI_TOTAL_COL] = formula;
                r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL]);
                r.Font.Bold = true;

                // Read the value back out of the same field we just wrote the formula int.
                // If that value is not equal to the bold ClickPay line at the top,
                // Make  it red.

                decimal mriTotal = decimal.Parse(r.Value.ToString());
                if (mriTotal != lastCPTotal)
                    r.Font.Color = Color.Red;

                if (mriTotal != lastCPTotal)
                {
                    ws.Cells[currentRow, MRI_TOTAL_COL + 1] = (mriTotal - lastCPTotal).ToString("C");
                    r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL + 1]);
                    r.Font.Bold = true;
                    r.Font.Color = Color.Red;
                    r.NumberFormat = "$#,###.00";
                }

                // Add totals to the top  of the period columns.  
                // They will  appear as extensions to the ClickPay Batch Row.
                beg = string.Format("{0}{1}", "R", beginBatchRow.ToString());
                end = string.Format("{0}{1}", "R", (currentRow - 1).ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 1] = formula;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 1]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";


                beg = string.Format("{0}{1}", "S", beginBatchRow.ToString());
                end = string.Format("{0}{1}", "S", (currentRow - 1).ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 2] = formula;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 2]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                beg = string.Format("{0}{1}", "T", beginBatchRow.ToString());
                end = string.Format("{0}{1}", "T", (currentRow - 1).ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 3] = formula;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 3]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                beg = string.Format("{0}{1}", "U", beginBatchRow.ToString());
                end = string.Format("{0}{1}", "U", (currentRow - 1).ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 4] = formula;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 4]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                beg = string.Format("{0}{1}", "V", beginBatchRow.ToString());
                end = string.Format("{0}{1}", "V", (currentRow - 1).ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 5] = formula;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 5]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";
            }
            else
            {
                // And set the cell value
                ws.Cells[currentRow, MRI_TOTAL_COL] = 0;
                r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL]);
                r.Font.Bold = true;

                decimal mriTotal = decimal.Parse(r.Value.ToString());
                if (mriTotal != lastCPTotal)
                    r.Font.Color = Color.Red;

                if (mriTotal != lastCPTotal)
                {
                    ws.Cells[currentRow, MRI_TOTAL_COL + 1] = 0;
                    r = ((Range)ws.Cells[currentRow, MRI_TOTAL_COL + 1]);
                    r.Font.Bold = true;
                    r.Font.Color = Color.Red;
                    r.NumberFormat = "$#,###.00";
                }

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 1] = 0;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 1]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 2] = 0;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 2]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 3] = 0;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 3]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";

                ws.Cells[beginBatchRow - 1, BANKRECID_COL + 4] = 0;
                r = ((Range)ws.Cells[beginBatchRow - 1, BANKRECID_COL + 4]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.NumberFormat = "$#,###.00";
            }

            currentRow += 2;        // Plus 2 to double  space between days.
        }

        public void WriteTransTotals()
        {
            // back  out the last increment
            currentRow--;

            // Do the Shortfall
            string beg = string.Format("{0}{1}", "P", 5);
            string end = string.Format("{0}{1}", "P", (currentRow - 1).ToString());
            string formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKRECID_COL - 1] = formula;
            Range r = ((Range)ws.Cells[currentRow, BANKRECID_COL - 1]);
            r.Font.Bold = true;
            r.Font.Color = Color.Red;
            r.NumberFormat = "$#,###.00";

            // The following renders the Bank Deposit Totals from
            // the grandTotalsByBankRecID Dictionary
            //
            // Headers
            int origCurrentRow = currentRow;    // Used to backfill the bankrec info grand totals.
            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN] = "Deposits";
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            currentRow++;

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2] = pi.FormattedPeriodWayBack;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.ColumnWidth = 15;

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1] = pi.FormattedPeriodPast;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.ColumnWidth = 15;

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN] = pi.FormattedPeriod;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.ColumnWidth = 15;

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1] = pi.FormattedPeriodFuture;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.ColumnWidth = 15;

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2] = pi.FormattedPeriodWayFore;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            r.ColumnWidth = 15;

            currentRow++;

            // Data
            int firstDataRow = currentRow;
            foreach (KeyValuePair<string, decimal[]> kvp in grandTotalsByBankRecID)
            {
                ws.Cells[currentRow, 1] = "Bank Deposit ID";
                r = ((Range)ws.Cells[currentRow, 1]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;

                ws.Cells[currentRow, BANKRECID_SUMMARY_COLUMN] = kvp.Key;
                r = ((Range)ws.Cells[currentRow, BANKRECID_SUMMARY_COLUMN]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2] = kvp.Value[1].ToString("C");
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1] = kvp.Value[2].ToString("C");
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN] = kvp.Value[3].ToString("C");
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1] = kvp.Value[4].ToString("C");
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2] = kvp.Value[5].ToString("C");
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2]);
                r.Font.Bold = true;
                r.Font.Size = 10;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                // Line Totals
                beg = string.Format("{0}{1}", "E", currentRow.ToString());
                end = string.Format("{0}{1}", "I", currentRow.ToString());
                formula = string.Format("=SUM({0}:{1})", beg, end);
                
                ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 3] = formula;
                r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 3]);
                r.Font.Bold = true;
                r.Font.Size = 12;
                r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

                currentRow++;
            }

            // Final Grand Total of Deposits
            beg = string.Format("{0}{1}", "E", currentRow.ToString());
            end = string.Format("{0}{1}", "I", currentRow.ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 3] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 3]);
            r.Font.Bold = true;
            r.Font.Size = 14;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
            r.ColumnWidth = 18;
            r.NumberFormat = "$#,###.00";


            // formula to SUM() the Deposits
            beg = string.Format("{0}{1}", "E", firstDataRow.ToString());
            end = string.Format("{0}{1}", "E", (currentRow - 1).ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 2]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            beg = string.Format("{0}{1}", "F", firstDataRow.ToString());
            end = string.Format("{0}{1}", "F", (currentRow - 1).ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN - 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            beg = string.Format("{0}{1}", "G", firstDataRow.ToString());
            end = string.Format("{0}{1}", "G", (currentRow - 1).ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            beg = string.Format("{0}{1}", "H", firstDataRow.ToString());
            end = string.Format("{0}{1}", "H", (currentRow - 1).ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            beg = string.Format("{0}{1}", "I", firstDataRow.ToString());
            end = string.Format("{0}{1}", "I", (currentRow - 1).ToString());
            formula = string.Format("=SUM({0}:{1})", beg, end);

            ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2] = formula;
            r = ((Range)ws.Cells[currentRow, BANKREC_DEPOSIT_CENTER_COLUMN + 2]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            // Backfill the column summaries of the BankRecID columns on far right.
            ws.Cells[origCurrentRow, BANKRECID_COL + 1] = string.Format("=E{0}", currentRow.ToString());
            r = ((Range)ws.Cells[origCurrentRow, BANKRECID_COL + 1]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            ws.Cells[origCurrentRow, BANKRECID_COL + 2] = string.Format("=F{0}", currentRow.ToString());
            r = ((Range)ws.Cells[origCurrentRow, BANKRECID_COL + 2]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            ws.Cells[origCurrentRow, BANKRECID_COL + 3] = string.Format("=G{0}", currentRow.ToString());
            r = ((Range)ws.Cells[origCurrentRow, BANKRECID_COL + 3]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            ws.Cells[origCurrentRow, BANKRECID_COL + 4] = string.Format("=H{0}", currentRow.ToString());
            r = ((Range)ws.Cells[origCurrentRow, BANKRECID_COL + 4]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;

            ws.Cells[origCurrentRow, BANKRECID_COL + 5] = string.Format("=I{0}", currentRow.ToString());
            r = ((Range)ws.Cells[origCurrentRow, BANKRECID_COL + 5]);
            r.Font.Bold = true;
            r.Font.Size = 12;
            r.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
        }

        public void SaveAndClose(PageInfo pi)
        {
            try
            {
                logger.LogMessage(string.Format("ExcelAgent.SaveAndClose - File: {0}", pi.ExcelFileName));

                wb.SaveAs(pi.ExcelFileName, XlFileFormat.xlWorkbookDefault);
                wb.Close();
                app.Quit();

                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(ws);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(app);
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in ExcelAgent.SaveAndClose: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
