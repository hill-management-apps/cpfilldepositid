﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class ReportDBAgent
    {
        Logger logger;

        private const string SQL_STMT_UPDATE =
            "update HS_PROCESSING " +
            "set Status = '{0}', " +
            "CompleteTime = '{1}', " +
            "CompletionMessage = '{2}' " +
            "where ID = {3} ";


        public ReportDBAgent(Logger _logger)
        {
            logger = _logger;
            logger.LogMessage("Here in ReportDBAgent ctor");
        }

        public DataSet GetSpecificRow(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.GetSpecificRow()");
            SqlAgent sa = new SqlAgent(logger);
            return sa.ExecuteQuery(string.Format("Select * from HS_PROCESSING where id = {0}", rowId));
        }

        public void SetInProgress(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetInProgress()");
            SqlAgent sa = new SqlAgent(logger);

            string sqlU = string.Format(SQL_STMT_UPDATE, "In Progess",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Empty, rowId);
            
            sa.ExecuteNonQuery(sqlU);
        }

        public void SetFailed(string msg, string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetFailed()");
            SqlAgent sa = new SqlAgent(logger);

            string sqlF = string.Format(SQL_STMT_UPDATE, "Failed",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), msg, rowId);
            
            sa.ExecuteNonQuery(sqlF);
        }

        public void SetComplete(string rowId)
        {
            logger.LogMessage("Here in ReportDBAgent.SetComplete()");
            SqlAgent sa = new SqlAgent(logger);

            string sqlS = string.Format(SQL_STMT_UPDATE, "Complete",
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), string.Empty, rowId);
            
            sa.ExecuteNonQuery(sqlS);
        }
    }
}
