﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;



namespace Utilities
{
    public class SqlAgent
    {
        private Logger logger = null;
        private string userId = string.Empty;
        const string CONNECTION_STRING = @"data source=HSSQL2012\HSSQLSERVER;initial catalog=HOMESALES;Integrated Security=true;";

        public SqlAgent(Logger _logger)
        {
            logger = _logger;
        }

        public DataSet ExecuteQuery(string sql)
        {
            return this.ExecuteQuery(sql, CONNECTION_STRING);
        }

        public DataSet ExecuteQuery(string sql, string connectString)
        {
            DataSet ds = new DataSet();

            try
            {
                SqlDataAdapter da = new SqlDataAdapter(sql, connectString);
                da.Fill(ds);
                return ds;
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteQuery: {0}", _ex.Message));
                throw _ex;
            }
        }

        public void ExecuteNonQuery(string sql)
        {
            ExecuteNonQuery(sql, CONNECTION_STRING);
        }

        public void ExecuteNonQuery(string sql, string connectionString)
        {
            logger.LogMessage("Here in SqlAgent.ExecuteNonQuery");

            try
            {
                SqlConnection cn = new SqlConnection(connectionString);
                SqlCommand command = new SqlCommand(sql, cn);
                command.Connection.Open();
                command.ExecuteNonQuery();
            }
            catch (Exception _ex)
            {
                logger.LogMessage(string.Format("=====> Error occured in SqlAgent.ExecuteNonQuery: {0}", _ex.Message));
                throw _ex;
            }
        }
    }
}
