﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public class PageInfo
    {
        public string LogFileName = string.Empty;
        public bool EchoToConsole = false;
        public string ExcelFileName = string.Empty;


        public string Period = string.Empty;
        public string PeriodPast = string.Empty;
        public string PeriodFuture = string.Empty;
        public string PeriodWayBack = string.Empty;
        public string PeriodWayFore = string.Empty;


        public PageInfo()
        {
        }

        public string FormattedPeriod
        {
            get { return ConvertPeriodToDate(this.Period); }
        }

        public string FormattedPeriodWayBack
        {
            get { return ConvertPeriodToDate(this.PeriodWayBack); }
        }

        public string FormattedPeriodWayFore
        {
            get { return ConvertPeriodToDate(this.PeriodWayFore); }
        }


        public string FormattedPeriodPast
        {
            get { return ConvertPeriodToDate(this.PeriodPast); }
        }

        public string FormattedPeriodFuture
        {
            get { return ConvertPeriodToDate(this.PeriodFuture); }
        }

        private string ConvertPeriodToDate(string period)
        {
            DateTime dt = (DateTime.Parse(string.Format("{0}/{1}/{2}",
                int.Parse(period.Substring(4, 2)), 1, int.Parse(period.Substring(0, 4)))));
            return dt.ToString("MMM-yyyy");
        }
    }
}
